from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class LinkAllStatusResponse(BaseModel):
    task_id: Optional[str]
    status: Optional[str]
    fail_message: Optional[str]


LinkAllStatusResponse.update_forward_refs()
