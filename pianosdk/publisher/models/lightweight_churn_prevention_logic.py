from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class LightweightChurnPreventionLogic(BaseModel):
    logic_id: Optional[str]
    name: Optional[str]
    default: Optional[bool]
    max_grace_period: Optional[int]


LightweightChurnPreventionLogic.update_forward_refs()
