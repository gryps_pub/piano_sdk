from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class TermLink(BaseModel):
    term_id: Optional[str]
    period_id: Optional[str]
    logic_id: Optional[str]
    name: Optional[str]
    default: Optional[bool]


TermLink.update_forward_refs()
