from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class SubscriptionMigrationHistoryDTO(BaseModel):
    subscription_id: Optional[str]
    schedule_time: Optional[datetime]
    from_term_name: Optional[str]
    is_migrated: Optional[bool]


SubscriptionMigrationHistoryDTO.update_forward_refs()
