from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional
from pianosdk.publisher.models.period_link_candidate import PeriodLinkCandidate
from typing import List


class LinkCandidate(BaseModel):
    term_id: Optional[str]
    name: Optional[str]
    type: Optional[str]
    payment_billing_plan_description: Optional[str]
    current_logic_id: Optional[str]
    current_logic_name: Optional[str]
    periods: Optional['List[PeriodLinkCandidate]']


LinkCandidate.update_forward_refs()
