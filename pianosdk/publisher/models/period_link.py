from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class PeriodLink(BaseModel):
    access_period_id: Optional[str]
    name: Optional[str]
    type: Optional[str]
    payment_billing_plan_description: Optional[str]
    removed: Optional[bool]


PeriodLink.update_forward_refs()
