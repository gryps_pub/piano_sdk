from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class Mandate(BaseModel):
    next_charge_date: Optional[datetime]
    reference: Optional[str]
    id: Optional[str]


Mandate.update_forward_refs()
