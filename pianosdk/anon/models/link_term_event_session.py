from datetime import date, datetime
from pydantic.main import BaseModel
from typing import Optional


class LinkTermEventSession(BaseModel):
    tracking_id: Optional[str]
    tbc: Optional[str]
    pcid: Optional[str]


LinkTermEventSession.update_forward_refs()
